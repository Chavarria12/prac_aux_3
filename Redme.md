#                 PRACTICA # 3
### Nomre: Jorge Eduardo Charria Condori

**1. Averigüe para que sirve la función “addEventListener”, y realice un ejemplo funcional en 
un archivo HTML con etiquetas “script”**

Rep. El método addEventlistener, es un escuchador que indica al navegador que este atento a la interacción del usuario. La ventaja es que se escribe totalmente en el JS, sin necesidad de tocar el HTML.


![Alt text](imagenes/img1.jpg)


**2. Averigüe y realice un ejemplo funcional de la creación, recorrido de posiciones y obtener la longitud de un vector en JS.**

![Alt text](imagenes/img2.jpg)

**3. Averigüe y realice ejemplos funcionales de los tipos de funciones que hay en JS**
### 1. String.split()

Divide una cadena en un arreglo de subcadenas de la cadena original a partir de un carácter separador:

![Alt text](imagenes/img3.jpg)


### 2. String.substring()
Extrae caracteres desde un índice A hasta un índice B sin incluirlo:

![Alt text](imagenes/img4.jpg)

### 3. String.trim()

Elimina espacios en blanco al inicio y al final de una cadena:

![Alt text](imagenes/img5.jpg)

### 4. Array.map()
Crea un arreglo nuevo con los resultados de la función enviada como parámetro:

![Alt text](imagenes/img6.jpg)

### 5. Array.push()
Agrega elementos al final de un arreglo y regresa la longitud del arreglo con los nuevos elementos:

![Alt text](imagenes/img7.jpg)

### 6. Array.pop()

el último elemento de un arreglo y lo devuelve:

![Alt text](imagenes/img8.jpg)

### 7. String.slice()
Regresa una porción de una cadena y devuelve una cadena nueva:

![Alt text](imagenes/img9.jpg)

### 8. Array.slice()
Regresa una porción de un arreglo y devuelve un arreglo nuevo:

![Alt text](imagenes/img10.jpg)

### 9. Object.toString()
Todos los objetos en JavaScript tienen este método y se usa para representarlos en forma de cadena de texto:


![Alt text](imagenes/img11.jpg)

### 10. Number.toFixed()
Establece la cantidad de decimales que queremos definir en un número:


![Alt text](imagenes/img12.jpg)

### 11. parseInt()
Convierte una cadena a un entero de la base especificada:


![Alt text](imagenes/img13.jpg)

### 12. Math.random()
Devuelve un número flotante aleatorio entre 0 y 1 (sin incluir el 1):

![Alt text](imagenes/img14.jpg)

### 13. console.log()
Escribe un mensaje en la consola web o en el intérprete de JavaScript:


![Alt text](imagenes/img15.jpg)
**4. Analice el siguiente código y encuentre el problema; luego mandar el código corregido.**

![Alt text](imagenes/img16.jpg)


**5. Cual es la diferencia entre JavaScript y TypeScript; y crea una variable, que almacene un número y luego hacer que se muestre en la consola en los dos lenguajes. Para ello, crear un archivo ts y js.**

Rep. TypeScript dispone de una escritura estática, mientras que JavaScript es un lenguaje dinámico. JavaScript no admite módulos, mientras que TypeScript sí que les da soporte. TypeScript dispone de interfaz, mientras que JavaScript no. En TypeScript sí que hay que compilar el código, en JavaScript no es necesario.

## javaScript

![Alt text](imagenes/img17.jpg)

## En typeScript


![Alt text](imagenes/img18.jpg)